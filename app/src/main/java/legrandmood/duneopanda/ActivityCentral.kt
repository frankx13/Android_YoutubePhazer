package legrandmood.duneopanda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.DriveScopes
import kotlinx.android.synthetic.main.activity_central.*
import legrandmood.duneopanda.adapters.DriveServiceHelper
import java.util.*

class ActivityCentral : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_central)


        setActionsListeners()
    }

    private fun setActionsListeners() {
        central_ytb_btn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        central_drv_btn.setOnClickListener {
            val intent = Intent(this, DriveActivity::class.java)
            startActivity(intent)
            finish()
        }
        login_btn_google_account.setOnClickListener {

        }
    }

    private fun setUpGoogleAccount(){
        val account = GoogleSignIn.getLastSignedInAccount(this)

        val credential = GoogleAccountCredential.usingOAuth2(
            this, Collections.singleton(DriveScopes.DRIVE_FILE)
        )
        credential.selectedAccount = account!!.account
        val googleDriveService = com.google.api.services.drive.Drive.Builder(
            AndroidHttp.newCompatibleTransport(),
            GsonFactory(),
            credential
        )
            .setApplicationName("Youtube Phazer")
            .build()
    }
}
