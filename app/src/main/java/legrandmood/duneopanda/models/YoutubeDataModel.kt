package legrandmood.duneopanda.models

class YoutubeDataModel {
    private var title = ""
    private var description = ""
    private var publishedAt = ""
    private var videoId = ""
    private var thumbNail = ""

    fun getVideoId(): String {
        return videoId
    }

    fun setVideoId(videoId: String) {
        this.videoId = videoId
    }

    fun getTitle(): String {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getDescription(): String {
        return description
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun getPublishedAt(): String {
        return publishedAt
    }

    fun setPublishedAt(publishedAt: String) {
        this.publishedAt = publishedAt
    }

    fun getThumbNail(): String {
        return thumbNail
    }

    fun setThumbNail(thumbNail: String) {
        this.thumbNail = thumbNail
    }
}