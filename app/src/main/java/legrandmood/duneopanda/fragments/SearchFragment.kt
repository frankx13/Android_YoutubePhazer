package legrandmood.duneopanda.fragments

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import legrandmood.duneopanda.R
import legrandmood.duneopanda.adapters.VideoPosTAdapter
import legrandmood.duneopanda.models.YoutubeDataModel
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.util.EntityUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*


class SearchFragment : Fragment() {
    private var GOOGLE_YOUTUBE_API_KEY = ""
    private var searchKeyword = ""
    private var CHANNEL_GET_URL = ""

    private var mList_videos: RecyclerView? = null
    private var adapter: VideoPosTAdapter? = null
    private var mListData = ArrayList<YoutubeDataModel>()
    private lateinit var bundleInput: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        mList_videos = view.findViewById(R.id.mList_videos)
        getSearchInput()
        initKeys()
        RequestYoutubeAPI().execute()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        exitSearch()
    }

    private fun getSearchInput() {
        val bundle = this.arguments
        val bundleSpaceComprised = bundle
        if (bundle != null) {
            bundleInput = bundle.getString("searchInput", "Cats%20meowing")
        } else {
            Toast.makeText(context, "Sorry, no results were found !", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initKeys() {
        GOOGLE_YOUTUBE_API_KEY = resources.getString(R.string.GOOGLE_API_KEY)
        searchKeyword = bundleInput
        searchKeyword = searchKeyword.replace(" ", "%20")
        CHANNEL_GET_URL = "https://www.googleapis.com/youtube/v3/search?" +
                "part=snippet" +
                "&q=$searchKeyword" +
                "&type=video" +
                "&key=$GOOGLE_YOUTUBE_API_KEY"
    }

    private fun initList(mListData: ArrayList<YoutubeDataModel>) {
        mList_videos!!.layoutManager = LinearLayoutManager(activity)
        adapter = VideoPosTAdapter(this.activity!!, activity!!.applicationContext, mListData)
        mList_videos!!.adapter = adapter
    }

    private fun parseVideoListFromResponse(jsonObject: JSONObject): ArrayList<YoutubeDataModel> {
        val mList = ArrayList<YoutubeDataModel>()
        if (jsonObject.has("items")) {
            try {
                val jsonArray = jsonObject.getJSONArray("items")
                for (i in 0 until jsonArray.length()) {
                    val youtube = YoutubeDataModel()
                    val json = jsonArray.getJSONObject(i)
                    if (json.has("id")) {
                        val jsonID = json.getJSONObject("id")
                        if (jsonID.has("kind")) {
                            if (jsonID.has("videoId")) {
                                val videoId = jsonID.getString("videoId")
                                youtube.setVideoId(videoId)
                            }
                            if (jsonID.getString("kind") == "youtube#video") {
                                //get data from snippet
                                val jsonSnippet = json.getJSONObject("snippet")
                                val title = jsonSnippet.getString("title")
                                val description = jsonSnippet.getString("description")
                                val publishedAt = jsonSnippet.getString("publishedAt")
                                val thumbnail =
                                    jsonSnippet.getJSONObject("thumbnails").getJSONObject("high")
                                        .getString("url")

//                                val youtube = YoutubeDataModel()
                                youtube.setTitle(title)
                                youtube.setDescription(description)
                                youtube.setPublishedAt(publishedAt)
                                youtube.setThumbNail(thumbnail)
                                mList.add(youtube)
                            }
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }

        return mList
    }

    //Asynctask to fetch data from YT
    private inner class RequestYoutubeAPI : AsyncTask<Void, String, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg voids: Void): String? {
            val httpClient = DefaultHttpClient()
            val httpGet = HttpGet(CHANNEL_GET_URL)
            Log.e("TESTOUM", "doInBackground: $CHANNEL_GET_URL")

            try {
                val response = httpClient.execute(httpGet)
                val httpEntity = response.entity
                return EntityUtils.toString(httpEntity)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

        override fun onPostExecute(response: String?) {
            super.onPostExecute(response)

            if (response != null) {
                try {
                    val jsonObject = JSONObject(response)
                    Log.e("RESPONSE", "onPostExecute: $jsonObject")
                    mListData = parseVideoListFromResponse(jsonObject)
                    initList(mListData)
                    Log.e("TESTOUM", "doInBackground: $mListData")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun exitSearch() {
        val exitBtn: ImageButton = activity!!.findViewById(R.id.exit_search_btn)
        exitBtn.setOnClickListener {
            activity!!.onBackPressed()
        }
    }
}
