package legrandmood.duneopanda

import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import kotlinx.android.synthetic.main.activity_main.*
import legrandmood.duneopanda.events.VideoIdEvent
import legrandmood.duneopanda.fragments.SearchFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.R.attr.name




class MainActivity : AppCompatActivity(), YouTubePlayer.OnInitializedListener {

    var YOUTUBE_PLAYLIST = "PLFE2CE09D83EE3E28"
    var YOUTUBE_VIDEO_ID = "_Yhyp-_hX2s"
    private val TAG = "YoutubeActivity"
    private lateinit var searchInput: String
    private lateinit var audioManager: AudioManager
    private var isPlayerRelease: Boolean = false
    private var isVideoMode: Boolean = true
    private var isFullScreen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //init audio service
        audioManager = this.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onStart() {
        super.onStart()
        //Register observer
        EventBus.getDefault().register(this)
        //init player
        val youTubePlayerFragment =
            supportFragmentManager.findFragmentById(R.id.youtube_IV_tv_inner) as YouTubePlayerSupportFragment
        youTubePlayerFragment.initialize(resources.getString(R.string.GOOGLE_API_KEY), this)
        setLaptopActions()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setLaptopActions() {
        laptop_ytb_video_btn.setOnClickListener {
            if (isVideoMode) {
                Toast.makeText(this, "You already are inside videos mode.", Toast.LENGTH_SHORT)
                    .show()
            } else {
                laptop_ytb_playlist_btn.scaleX = 0.8f
                laptop_ytb_playlist_btn.scaleY = 0.8f
                laptop_ytb_video_btn.scaleY = 1f
                laptop_ytb_video_btn.scaleX = 1f
                laptop_ytb_video_btn.background =
                    getDrawable(R.drawable.action_btn_laptop_youtube_selected)
                laptop_ytb_playlist_btn.background =
                    getDrawable(R.drawable.action_btn_laptop_youtube)
                isVideoMode = true
            }
        }
        laptop_ytb_playlist_btn.setOnClickListener {
            if (isVideoMode) {
                laptop_ytb_playlist_btn.scaleX = 1f
                laptop_ytb_playlist_btn.scaleY = 1f
                laptop_ytb_video_btn.scaleY = 0.8f
                laptop_ytb_video_btn.scaleX = 0.8f
                laptop_ytb_video_btn.background = getDrawable(R.drawable.action_btn_laptop_youtube)
                laptop_ytb_playlist_btn.background =
                    getDrawable(R.drawable.action_btn_laptop_youtube_selected)
                isVideoMode = false
            } else {
                Toast.makeText(this, "You already are inside playlist mode.", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun enableDisableView(view: View, enabled: Boolean) {
        view.isEnabled = enabled
        if (view is ViewGroup) {
            for (idx in 0 until view.childCount) {
                enableDisableView(view.getChildAt(idx), enabled)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onInitializationSuccess(
        provider: YouTubePlayer.Provider?,
        youTubePlayer: YouTubePlayer?,
        wasRestored: Boolean
    ) {
        Log.d(TAG, "onInitializationSuccess : provider is ${provider?.javaClass}")
        Log.d(TAG, "onInitializationSuccess : youTubePlayer is ${youTubePlayer?.javaClass}")
        Toast.makeText(this, "SUCCESS", Toast.LENGTH_LONG).show()

        //TODO Figure out why the activity is recreated in full screen, causing the enableDisableView method to be enabled
        //TODO because isFullScreen is not set to true
//        enableDisableView(useless_view, false)
        youTubePlayer?.setPlayerStateChangeListener(playerStateChangerListener)
        youTubePlayer?.setPlaybackEventListener(playbackEventListener)
        youTubePlayer?.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS)
        youTubePlayer?.setOnFullscreenListener {
            //            enableDisableView(useless_view, true)
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)
        }

        if (!wasRestored) {
            youTubePlayer?.loadVideo(YOUTUBE_VIDEO_ID)
        } else {
            youTubePlayer?.play()
        }

        if (youTubePlayer != null) onClickActionEvents(youTubePlayer)
    }

    override fun onInitializationFailure(
        provider: YouTubePlayer.Provider?,
        youTubeInitializationResult: YouTubeInitializationResult?
    ) {
        val REQUEST_CODE = 0

        if (youTubeInitializationResult?.isUserRecoverableError == true)
            youTubeInitializationResult.getErrorDialog(this, REQUEST_CODE).show()
        else {
            val errorMessage =
                "There was an error initizalizing the YoutubePlayer : ($youTubeInitializationResult)"
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
        }
    }

    private val playbackEventListener = object : YouTubePlayer.PlaybackEventListener {
        override fun onSeekTo(p0: Int) {

        }

        override fun onBuffering(p0: Boolean) {

        }

        override fun onPlaying() {
            Toast.makeText(this@MainActivity, "Video is playing", Toast.LENGTH_LONG).show()
        }

        override fun onStopped() {
            Toast.makeText(this@MainActivity, "Video has stopped", Toast.LENGTH_LONG).show()
        }

        override fun onPaused() {
            Toast.makeText(this@MainActivity, "Video has paused", Toast.LENGTH_LONG).show()
        }
    }

    private val playerStateChangerListener = object : YouTubePlayer.PlayerStateChangeListener {
        override fun onAdStarted() {
            Toast.makeText(this@MainActivity, "Video ad", Toast.LENGTH_LONG).show()
        }

        override fun onLoading() {

        }

        override fun onVideoStarted() {
            Toast.makeText(this@MainActivity, "Video has started", Toast.LENGTH_LONG).show()
        }

        override fun onLoaded(p0: String?) {

        }

        override fun onVideoEnded() {
            Toast.makeText(this@MainActivity, "Video has ended", Toast.LENGTH_LONG).show()
        }

        override fun onError(p0: YouTubePlayer.ErrorReason?) {

        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun onClickActionEvents(player: YouTubePlayer) {
        play_button.setOnClickListener {
            if (!isPlayerRelease) {
                play_button.background = getDrawable(R.drawable.action_btn_design_selected)
                stop_button.background = getDrawable(R.drawable.action_btn_design)
                pause_button.background = getDrawable(R.drawable.action_btn_design)
                player.play()
            } else {
                val youTubePlayerFragment =
                    supportFragmentManager.findFragmentById(R.id.youtube_IV_tv_inner) as YouTubePlayerSupportFragment
                youTubePlayerFragment.initialize(resources.getString(R.string.GOOGLE_API_KEY), this)
                player.play()
                isPlayerRelease = false
            }
        }
        pause_button.setOnClickListener {
            if (!isPlayerRelease) {
                play_button.background = getDrawable(R.drawable.action_btn_design)
                stop_button.background = getDrawable(R.drawable.action_btn_design)
                pause_button.background = getDrawable(R.drawable.action_btn_design_selected)
                player.pause()
            } else {
                Toast.makeText(
                    this,
                    "The video stopped. Click on PLAY or search a video to launch the Player.",
                    Toast.LENGTH_LONG
                ).show()
            }

        }
        stop_button.setOnClickListener {
            if (!isPlayerRelease) {
                play_button.background = getDrawable(R.drawable.action_btn_design)
                stop_button.background = getDrawable(R.drawable.action_btn_design_selected)
                pause_button.background = getDrawable(R.drawable.action_btn_design)
                player.release()
                isPlayerRelease = true
            } else {
                Toast.makeText(this, "The video has already been stopped.", Toast.LENGTH_SHORT)
                    .show()
            }

        }
        volume_up_button.setOnClickListener {
            audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND)
            var currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
            Toast.makeText(this, currentVolume.toString(), Toast.LENGTH_SHORT).show()
        }
        volume_down_button.setOnClickListener {
            audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND)
            var currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
            Toast.makeText(this, currentVolume.toString(), Toast.LENGTH_SHORT).show()
        }
        search_button.setOnClickListener {
            play_button.background = getDrawable(R.drawable.action_btn_design)
            stop_button.background = getDrawable(R.drawable.action_btn_design)
            pause_button.background = getDrawable(R.drawable.action_btn_design)
            search_button.background = getDrawable(R.drawable.action_btn_design_selected)
            laptop_ytb_video_btn.visibility = View.GONE
            laptop_ytb_playlist_btn.visibility = View.GONE
            player.release()
            isPlayerRelease = true

            search_input_bar.visibility = View.VISIBLE
            search_validation_bar.visibility = View.VISIBLE
        }
        search_validation_bar.setOnClickListener {
            play_button.background = getDrawable(R.drawable.action_btn_design_selected)
            search_button.background = getDrawable(R.drawable.action_btn_design)

            searchInput = search_input_bar.editableText.toString()
            search_input_bar.visibility = View.GONE
            search_validation_bar.visibility = View.GONE
            fragment_screen.visibility = View.VISIBLE

            searchResultScreen(searchInput)
        }

        fullscreen_on_button.setOnClickListener {
            if (!isPlayerRelease) {
                isFullScreen = true
                player.setFullscreen(true)
            } else {
                Toast.makeText(
                    this,
                    "The video stopped. Click on PLAY or search a video to launch the Player.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        next_video_button.setOnClickListener {
            if (!isPlayerRelease) {
                if (player.hasNext()) {
                    play_button.background = getDrawable(R.drawable.action_btn_design)
                    stop_button.background = getDrawable(R.drawable.action_btn_design)
                    pause_button.background = getDrawable(R.drawable.action_btn_design)
                    search_button.background = getDrawable(R.drawable.action_btn_design)
                    next_video_button.background =
                        getDrawable(R.drawable.action_btn_design_selected)
                    search_button.background = getDrawable(R.drawable.action_btn_design)
                    player.next()
                } else {
                    Toast.makeText(
                        this,
                        "No other videos are following inside this playing. You can search for a new one.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    this,
                    "The video stopped. Click on PLAY or search a video to launch the Player.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        previous_video_button.setOnClickListener {
            if (!isPlayerRelease) {
                if (player.hasPrevious()) {
                    play_button.background = getDrawable(R.drawable.action_btn_design)
                    stop_button.background = getDrawable(R.drawable.action_btn_design)
                    pause_button.background = getDrawable(R.drawable.action_btn_design)
                    search_button.background = getDrawable(R.drawable.action_btn_design)
                    next_video_button.background = getDrawable(R.drawable.action_btn_design)
                    search_button.background = getDrawable(R.drawable.action_btn_design_selected)
                    player.previous()
                } else {
                    Toast.makeText(
                        this,
                        "You didn't watch any videos before this one, it's impossible to play the previous one.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    this,
                    "The video stopped. Click on PLAY or search a video to launch the Player.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        //TODO Follow those 3 steps in order to make google account for youtube testable :
        /*E/DEBUG ACCOUNT: AuthenticatorException: UNREGISTERED_ON_API_CONSOLE
        To fix it, we must :
        1- Sign your APK, manually or through Gradle or whatever: the Android documentation is pretty clear on this step;
        2- Get your sha1 fingerprint; as mention in this SO answer, it's kind of easy on Android Studio: in the Gradle panel, select the signingReport task under your root project and run it - the SHA1 fingerprint will show in the text output;
        3- Register your APK through the Google dev console: create a new Credentials / OAuth client id / Android, defined by the SHA1 fingerprint you got and your APK package name.

       log_in_btn.setOnClickListener {
            isPlayerRelease = true
            player.release()
            AccountManager.get(applicationContext).getAuthTokenByFeatures(
                "com.google",
                "oauth2:https://gdata.youtube.com",
                null,
                this,
                null,
                null,
                { future ->
                    try {
                        val bundle = future.result
                        val acc_name = bundle.getString(AccountManager.KEY_ACCOUNT_NAME)
                        val auth_token = bundle.getString(AccountManager.KEY_AUTHTOKEN)

                        Log.e("DEBUG ACCOUNT", "name: $acc_name; token: $auth_token")

                    } catch (e: Exception) {
                        Log.e("DEBUG ACCOUNT", e.javaClass.simpleName + ": " + e.message)
                    }
                },
                null
            )
        }
         */

        exit_youtube_btn_laptop.setOnClickListener {
            Toast.makeText(this, "Leaving Youtube...", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, ActivityCentral::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun searchResultScreen(userInputSearch: String) {
        val searchFragment = SearchFragment()
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()

        val bundle = Bundle()
        bundle.putString("searchInput", userInputSearch)
        searchFragment.arguments = bundle

        transaction.add(R.id.fragment_screen, searchFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        Log.e("STACK", supportFragmentManager.backStackEntryCount.toString())
        supportFragmentManager.popBackStackImmediate()
    }

    //Capture l'event avec cette signature
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onVideoIdEvent(event: VideoIdEvent) {
        fragment_screen.visibility = View.GONE
        YOUTUBE_VIDEO_ID = event.getVidId()
        Log.e("EVENTBUS", event.getVidId())
        isPlayerRelease = false
        val youTubePlayerFragment =
            supportFragmentManager.findFragmentById(R.id.youtube_IV_tv_inner) as YouTubePlayerSupportFragment
        youTubePlayerFragment.initialize(resources.getString(R.string.GOOGLE_API_KEY), this)
    }

    override fun onStop() {
        super.onStop()
        //unregister to eventbus
        EventBus.getDefault().unregister(this)
    }
}
