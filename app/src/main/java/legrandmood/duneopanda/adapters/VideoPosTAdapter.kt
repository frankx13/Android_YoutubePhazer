package legrandmood.duneopanda.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import legrandmood.duneopanda.R
import legrandmood.duneopanda.events.VideoIdEvent
import legrandmood.duneopanda.models.YoutubeDataModel
import org.greenrobot.eventbus.EventBus
import java.util.*


class VideoPosTAdapter(
    activity: FragmentActivity,
    mContext: Context,
    private val dataSet: ArrayList<YoutubeDataModel>
) :
    RecyclerView.Adapter<VideoPosTAdapter.YoutubePostHolder>() {
    var mContext: Context? = null
    var activity: FragmentActivity? = null

    init {
        this.mContext = mContext
        this.activity = activity
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YoutubePostHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.youtube_post_layout, parent, false)
        return YoutubePostHolder(view)
    }

    override fun onBindViewHolder(holder: YoutubePostHolder, position: Int) {
        val textViewTitle = holder.textViewTitle
        val textViewDes = holder.textViewDes
        val textViewDate = holder.textViewDate
        val videoId = dataSet[position].getVideoId()
        val imageThumb = holder.imageThumb
        val itemContainer = holder.itemContainer
        val `object` = dataSet[position]

        textViewTitle.text = `object`.getTitle()
        textViewDes.text = `object`.getDescription()
        textViewDate.text = `object`.getPublishedAt()

        Picasso.with(mContext).load(`object`.getThumbNail()).into(imageThumb)

        itemContainer.setOnClickListener {

            postOnEventBus(videoId)

            activity!!.onBackPressed()
        }
    }

    private fun postOnEventBus(videoId: String) {
        val event:VideoIdEvent = VideoIdEvent(videoId)
        event.setVidId(videoId)
        EventBus.getDefault().postSticky(event)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    class YoutubePostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textViewTitle: TextView = itemView.findViewById(R.id.textViewTitle)
        internal var textViewDes: TextView = itemView.findViewById(R.id.textViewDes)
        internal var textViewDate: TextView = itemView.findViewById(R.id.textViewDate)
        internal var imageThumb: ImageView = itemView.findViewById(R.id.imageThumb)
        internal var itemContainer: LinearLayout = itemView.findViewById(R.id.container_item)
    }
}