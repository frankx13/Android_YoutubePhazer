package legrandmood.duneopanda.holders

import com.google.api.client.util.DateTime

class GoogleDriveFileHolder {
    private var id: String? = null
    private var name: String? = null
    private var modifiedTime: DateTime? = null
    private var size: Long = 0
    private var createdTime: DateTime? = null
    private var starred: Boolean? = null


    fun getCreatedTime(): DateTime? {
        return createdTime
    }

    fun setCreatedTime(createdTime: DateTime) {
        this.createdTime = createdTime
    }

    fun getStarred(): Boolean? {
        return starred
    }

    fun setStarred(starred: Boolean?) {
        this.starred = starred
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getModifiedTime(): DateTime? {
        return modifiedTime
    }

    fun setModifiedTime(modifiedTime: DateTime) {
        this.modifiedTime = modifiedTime
    }

    fun getSize(): Long {
        return size
    }

    fun setSize(size: Long) {
        this.size = size
    }
}